## Notes by MaPas -- start

JAXBER 

create-react-app jaxber

run local, code and test
- nmp start

create publish version
- npm run build

used and tested npms
- npm install --save redux react-redux
- npm install --save @material-ui/core
- npm install @material-ui/icons --save
- npm install --save react-router-dom
- npm install --save redux-thunk
- npm install axios --save
- npm install --save moment react-moment
- npm install react-player --save <- video problems in iOS!
- npm install --save video-react <- css problems
- npm install react-plyr --save <- problem in iOS
- npm install react-html5video --save <- problem in iOA
- npm install react-media-player --save
- npm install --save iphone-inline-video
- npm install --save react-html5-camera-photo

REDUX
******

npm install --save redux react-redux

actions-folder
- actions

reducers-folder
- reducers

components-folder
- React UI class or function

containers-folder
- "smart components"
- React component which connect to a redux store
- receive Redux state updates and dispatch action
- usually don't render DOM elements

store
- hold "states"
- npm i react-redux --save-dev
- muutokset src/index.js 
- mkdir -p src/js/components komponentit

install
- npm i redux --save-dev

create store
- mkdir -p src/js/store
- and index.js

create reducer
- mkdir -p src/js/reducers
- and index.js

create actions
- mkdir -p src/js/actions
- and index.js

create constants
- mkdir -p src/js/constants
- and action-types.js

start chrome without sandbox

open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_test" --disable-web-security

login: pasi
passwd: pasi

App.js
- routes:
  path="/" CreateLoginForm
  path="/campaigns" CreateCampaigns
  path="/campaign" CreateCampaign
  path="/addcampaignitem" CreateAddCampaignItem
  path="/register" CreateRegisterForm

1. actions
- redux actions 

2. components
- normal React own made components

3. containers
 - container component is a component that is responsible for retrieving data, 
 - and in order to get that data, the component needs to use Redux's connect 
 - and mapStateToProps functions.

4. reducers
- Redux reducers, combine reducers and store

## Notes by MaPas -- end

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
