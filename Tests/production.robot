*** Settings ***
Library    SeleniumLibrary
Suite Teardown  Close Browser


*** Test Cases ***
Test Chrome
    Open Browser        https://saadturky.gitlab.io/react-pwa-version      Chrome      options=add_argument("--disable-popup-blocking"); add_argument("--ignore-certificate-errors"); add_argument("--disable-dev-shm-usage"); add_argument("--no-sandbox"); add_argument("--headless")
    Set Selenium Speed  2
    Location Should Be  https://saadturky.gitlab.io/react-pwa-version/#/
    Title Should Be     Jaxber
	Page Should Contain Button       Register
	Click Button        Register
	Wait Until Page Contains    Register in Jaxber                timeout=10
    [Teardown]          Close All Browsers

Test Firefox
    Open Browser        https://saadturky.gitlab.io/react-pwa-version      Firefox     options=add_argument("--headless")
    Set Selenium Speed  2
    Location Should Be  https://saadturky.gitlab.io/react-pwa-version/#/
    Title Should Be     Jaxber
	Page Should Contain Button       Register
	Click Button        Register
	Wait Until Page Contains    Register in Jaxber                timeout=10
    [Teardown]          Close All Browsers