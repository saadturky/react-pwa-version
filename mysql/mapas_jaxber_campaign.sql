-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: mysql.labranet.jamk.fi    Database: mapas
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jaxber_campaign`
--

DROP TABLE IF EXISTS `jaxber_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jaxber_campaign` (
  `idcampaign` int(11) NOT NULL AUTO_INCREMENT,
  `created` varchar(45) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `header` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `video` varchar(45) DEFAULT NULL,
  `text` varchar(500) DEFAULT NULL,
  `idcampaigns` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcampaign`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jaxber_campaign`
--

LOCK TABLES `jaxber_campaign` WRITE;
/*!40000 ALTER TABLE `jaxber_campaign` DISABLE KEYS */;
INSERT INTO `jaxber_campaign` VALUES (1,'2019/01/01',1,'Sample header','project3.jpg',NULL,'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, \nwhen an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electron\nic typesetting, remaining essentially unchanged.',8),(2,'2019/01/19',1,'Just a QuickTime',NULL,'2019.syksy.mobiilikurssit.mov','This is just a QuickTime movie test, hope it plays',8),(3,'2019/01/20',2,'Just a MP4',NULL,'big_buck_bunny.mp4','There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised\n words which don\'t look even slightly believable.',8),(4,'2019/02/07',3,'Info','project2.jpg',NULL,'This is just a test, hope this text is visible!',8),(5,'2019/02/09',3,'What?','project1.jpg',NULL,'Here is a sample text in one campaign item.',8),(29,'2019/02/19',4,'Image iOS','1550574836.jpg','','Testing image material',24),(30,'2019/02/19',4,'Testing video iOS','','1550574907.MOV','Ok let see how video recording and playing works!',24),(31,'2019/02/19',4,'Image from Android','1550575061.jpg','','Ok mites Ã¤Ã¤kkÃ¶set tekstissÃ¤??',24),(32,'2019/02/19',4,'Video @@ android','','1550575131.mp4','Juu ja nÃ¤tisti menee!',24),(33,'2019/02/19',4,'Test','','','asd',35),(34,'2019/02/19',4,'Testi','1550595166.jpg','','Testiteksti',36),(35,'2019/02/19',9,'Vau','1550599915.jpg','','it is getting better and better',24),(36,'2019/03/07',4,'Testi','1551943557.jpg','','Testi',24),(37,'2019/03/10',4,'Test','','','comment ',8),(38,'2019/03/10',4,'Test','','1552225269.MOV','comment ',8),(39,'2019/06/05',4,'Jones','1559722785.jpg','','Jones',8),(40,'2019/06/05',10,'Test','','','Test',37),(41,'2019/08/15',4,'Kisaa','1565861798.jpg','','Koira',24),(42,'2019/10/29',4,'Hyyhh','1572348920.jpg','','Hyhjjkkk',37),(43,'2019/10/29',4,'Test topic','1572348922.jpg','','Some descriptions ',37);
/*!40000 ALTER TABLE `jaxber_campaign` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-29 14:48:38
