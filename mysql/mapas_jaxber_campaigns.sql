-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: mysql.labranet.jamk.fi    Database: mapas
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jaxber_campaigns`
--

DROP TABLE IF EXISTS `jaxber_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jaxber_campaigns` (
  `idcampaigns` int(11) NOT NULL AUTO_INCREMENT,
  `created` varchar(45) DEFAULT NULL,
  `lastActivity` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `private` tinyint(4) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcampaigns`),
  KEY `jaxberuser_idx` (`iduser`),
  CONSTRAINT `jaxberuser` FOREIGN KEY (`iduser`) REFERENCES `jaxber_users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jaxber_campaigns`
--

LOCK TABLES `jaxber_campaigns` WRITE;
/*!40000 ALTER TABLE `jaxber_campaigns` DISABLE KEYS */;
INSERT INTO `jaxber_campaigns` VALUES (1,'2018/02/02','2018/03/03','JKL Messut',0,1),(2,'2018/05/11','2019/01/01','JaxBerit',1,2),(3,'2018/05/12','2018/06/06','HipHop',0,3),(4,'2018/09/01','2018/10/11','Games on Ice',1,1),(5,'2018/09/10','2018/10/10','IT-Boys',0,2),(6,'2018/12/10','2018/12/24','Cyberi Party',1,3),(7,'2019/01/22','2019/01/30','Mobile Programming Module',0,1),(8,'2019/02/09','2019/02/11','JAMK 2030',0,2),(24,'2019/02/19','2019/02/19','MaPas Campaign',0,4),(37,'2019/06/05','2019/06/05','Testi',0,10);
/*!40000 ALTER TABLE `jaxber_campaigns` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-29 14:48:40
