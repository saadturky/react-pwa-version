-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: mysql.labranet.jamk.fi    Database: mapas
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jaxber_comments`
--

DROP TABLE IF EXISTS `jaxber_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jaxber_comments` (
  `idcomment` int(11) NOT NULL AUTO_INCREMENT,
  `idcampaign` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `created` varchar(45) DEFAULT NULL,
  `text` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcomment`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jaxber_comments`
--

LOCK TABLES `jaxber_comments` WRITE;
/*!40000 ALTER TABLE `jaxber_comments` DISABLE KEYS */;
INSERT INTO `jaxber_comments` VALUES (1,5,1,'2019/02/16','Here is first test comment.'),(2,5,2,'2019/02/18','Another comment here, so there should be two '),(8,29,4,'2019/02/19','TÃ¤ssÃ¤ ensimmÃ¤inen kommentti.'),(12,29,4,'2019/02/19','TÃ¤ssÃ¤ toinen kommentti.'),(16,30,4,'2019/02/19','Video ei nÃ¤y iOSllÃ¤!!'),(17,34,4,'2019/02/19','First comment'),(18,30,4,'2019/02/19','Hyvin nÃ¤kyy Androidissa!'),(19,35,9,'2019/02/19','testiÃ¤ pukkaa'),(20,35,4,'2019/02/19','HyvÃ¤ Jones!'),(21,39,4,'2019/06/05','Juu'),(22,41,4,'2019/10/28','Nice!!!');
/*!40000 ALTER TABLE `jaxber_comments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-29 14:48:43
