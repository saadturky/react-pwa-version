<?php

$filetype = $_POST['filetype'];

if (!isset($_POST['filetype'])) {
  echo '{"status":"error","message":"Error uploading campaign item!"}';
  return;
}


$date = new DateTime();
$seconds = $date->getTimestamp();
$filename = "";
$video = null;
$image = null;

if (isset($_FILES['file'])) {
  $path_parts = pathinfo($_FILES['file']['name']);
  $ext = $path_parts['extension'];
  $filename = $seconds.".".$ext;
  //$filename = $seconds.$_FILES['file']['name'];

  move_uploaded_file($_FILES['file']['tmp_name'], "./".$filetype."/".$filename);

  if ($filetype == 'images') $image = $filename;
  if ($filetype == 'videos') $video = $filename;
}

// add campaign item
$servername = "mysql.labranet.jamk.fi";
$username = "mapas";
$password = "yourpasswordhere";
$dbname = "mapas";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$iduser = $_POST['iduser'];
$idcampaigns = $_POST['idcampaigns'];
$created = $_POST['created'];
$header = $_POST['header'];
$text = $_POST['text'];

$sql = "INSERT INTO jaxber_campaign (created, iduser, header, image, video, text, idcampaigns) VALUES ('$created', '$iduser', '$header','$image', '$video', '$text', '$idcampaigns')";

if ($conn->query($sql) === TRUE) {
    echo '{"status":"ok","message":"Campaign item added successfully!"}';
} else {
   echo '{"status":"error","message":"Error with adding a Campaign item!"}';
}

$conn->close();

?>

