<?php

$servername = "mysql.labranet.jamk.fi";
$username = "mapas";
$password = "yourpasswordhere";
$dbname = "mapas";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$idcampaign = $_GET['idcampaign'];

// $sql = "SELECT * FROM jaxber_campaigns t1 inner join jaxber_users t2 on (t1.iduser = t2.idusers)  ORDER BY t1.created DESC";
// $sql = "SELECT * FROM jaxber_campaign ORDER BY created WHERE idcampaigns='$idcampaign' DESC";
$sql = "SELECT * FROM jaxber_campaign t1 inner join jaxber_users t2 on (t1.iduser = t2.idusers) WHERE idcampaigns='$idcampaign' ORDER BY created DESC";
$result = $conn->query($sql);

$dbdata = array();
while ( $row = $result->fetch_assoc())  {
  $dbdata[]=$row;
}

echo json_encode($dbdata);

$conn->close();

?>
