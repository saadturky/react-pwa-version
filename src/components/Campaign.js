import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import AddIcon from '@material-ui/icons/Add';
import CampaignCard from './CampaignCard';

class Campaign extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            expanded: false 
        };
        
        // start loading campaign
        this.props.loadCampaign(this.props.campaigns[this.props.selectedCampaignIndex]);
    }

    addCampaignItem = () => {
      //console.log("iduser="+this.props.iduser);
      //console.log("idcampaigns="+this.props.campaigns[this.props.selectedCampaignIndex].idcampaigns);
      this.props.history.push({"pathname": "/AddCampaignItem", state: { iduser: this.props.iduser, idcampaigns: this.props.campaigns[this.props.selectedCampaignIndex].idcampaigns}});
    }

    back = () => {
        this.props.history.goBack();
    }

    render() {
        const { classes } = this.props;
        let { 
            isLoadingCampaignPending, 
            isLoadingCampaignSuccess, 
            loadingCampaignError, 
            campaign, 
            campaigns,
            selectedCampaignIndex,
            iduser} = this.props;

        var campaingName = '';
        var items = null;
        if (isLoadingCampaignSuccess) {
            if (campaigns.length > 0) campaingName = campaigns[selectedCampaignIndex].name;
            if (campaign.length > 0) {
                items = campaign.map((campaignItem, index) => {
                    return (
                        <CampaignCard campaignItem={campaignItem} key={index} iduser={iduser}/>
                    );
                  })}
        }

        return (
            <main className={classes.main}>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" onClick={_ => this.back()}>
                            <ArrowLeftIcon />
                        </IconButton>
                        <Typography variant="overline" color="inherit" className={classes.grow} noWrap={true}>
                            Jaxber - {campaingName}
                        </Typography><br/>
                        <IconButton color="inherit" onClick={_ => this.addCampaignItem()}>
                            <AddIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>

                {isLoadingCampaignPending && 
                    <p>Loading campaign, please wait...</p>
                }
                {loadingCampaignError != null &&
                    <p>Campaign loading error:<br/>- {loadingCampaignError}</p>
                }
                { (isLoadingCampaignSuccess && items == null) && 
                    <p>There is no campaign items!</p>
                }
                { (isLoadingCampaignSuccess && items != null) && 
                    <List>
                        {items}
                    </List>
                }
            </main>
        )
    }
}

const styles = theme => ({
    grow: {
        flexGrow: 1,
      },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        //marginLeft: theme.spacing.unit * 3,
        //marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
          width: 400,
          marginLeft: 'auto',
          marginRight: 'auto',
        },
      },
    card: {
      maxWidth: 400,
      marginTop:5
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    actions: {
      display: 'flex',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    lockIcon: {
      float:'right'
    },
    avatar: {
        backgroundColor: red[500],
      },
  });

  Campaign.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  

export default withStyles(styles)(Campaign);
