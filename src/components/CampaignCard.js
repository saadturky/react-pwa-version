import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ReactPlayer from 'react-player'
import axios from 'axios';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';

//import { Player } from 'video-react';
//import "video-react/dist/video-react.css";
// import Plyr from 'react-plyr';
//import { DefaultPlayer as Video } from 'react-html5video';
//import 'react-html5video/dist/styles.css';
//import { Media, Player, controls } from 'react-media-player'
//import enableInlineVideo from 'iphone-inline-video';

class CampaignCard extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            expanded: false,
            favorited: false,
            comments : null,
            loadingComments: false,
            anchorEl: null,
            commentDialogOpen: false,
            commenttext: '',
            isSendingComment: false,
            sendingMessage: ''
        };

        this.updateCommenttextValue = this.updateCommenttextValue.bind(this);
    }

    updateCommenttextValue(e) {
        this.setState({ commenttext: e.target.value });
      } 

    showCommentDialog = () => {
        this.setState({ commentDialogOpen: true });
    };
    
    closeCommentDialog = () => {
        this.setState({ commentDialogOpen: false });
    };

    handleExpandClick = (idcampaign) => {
        //console.log(idcampaign);
        // start loading comments
        if (!this.state.expanded) {
            this.setState({ expanded: !this.state.expanded, loadingComments: true });
            this.loadComments(idcampaign);
        } else {
            this.setState({ expanded: !this.state.expanded, loadingComments: false });
        }
    };

    loadComments(idcampaign) {
        var self = this;
        axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/getComments.php?idcampaign='+idcampaign)
            .then(function (response) {
                self.setState({comments: response.data, loadingComments: false});
            })
            .catch(function (error) {
                self.setState({comments: null, loadingComments: false});
            });  
    }

    favorite = () => {
        this.setState(state => ({ favorited: !state.favorited }));
    }

    handleCardMenuOpen = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleCardMenuClose = () => {
        this.setState({ anchorEl: null });
    }

    openAddCommentDialog = () => {
        this.handleCardMenuClose();
        this.setState({commenttext: ''});
        this.showCommentDialog();
    }

    addNewComment = () => {
        //console.log("add comment");
        //console.log(this.props.campaignItem.idcampaign);
        //console.log(this.props.iduser);
        //console.log(this.state.commenttext);

        const moment = require('moment');
        let date = new Date();
        let created = moment(date.toISOString()).format('YYYY/MM/DD');

        this.setState({isSendingComment: true, sendingMessage: 'Sending, please wait...'});
        var self = this;
        axios.get('https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/addComment.php'
                +'?idcampaign='+this.props.campaignItem.idcampaign
                +'&iduser='+this.props.iduser
                +'&text='+this.state.commenttext
                +'&created='+created)
            .then(function (response) {
                //console.log(response.data);
                self.setState({isSendingComment: false});
                self.closeCommentDialog();
                //if (!self.state.expanded) self.setState({expanded: true});
                //elf.loadComments();
                //self.handleExpandClick(self.props.campaignItem.idcampaign);
                if (!self.state.expanded) {
                    self.setState({ expanded: !self.state.expanded, loadingComments: true });
                }
                self.loadComments(self.props.campaignItem.idcampaign);
            })
            .catch(function (error) {
                //console.log(error);
                self.setState({isSendingComment: false});
                self.closeCommentDialog();
                alert(error);
            }); 
    }

    render() {
        const { anchorEl } = this.state;
        const { classes } = this.props;
        const moment = require('moment');
        // created
        let date = new Date(this.props.campaignItem.created);
        let created = moment(date.toISOString()).format('YYYY/MM/DD');
        let subheader = this.props.campaignItem.username + " " + created;

        var comments;
        if (this.state.loadingComments) {
            comments = <div className={classes.comment}><Typography paragraph>Loading comments...</Typography></div>;
        }
        else if (this.state.comments === null) {
            comments = <div className={classes.comment}><Typography paragraph>No comments.</Typography></div>;
        } 
        else if (this.state.comments.length === 0) {
            comments = <div className={classes.comment}><Typography paragraph>No comments.</Typography></div>;
        } 
        else if (this.state.comments.length > 0) {
            comments = this.state.comments.map((comment, index) => {
                let commentHeader = comment.username + " " + comment.created;
                return (
                    <div key={index} className={classes.comment}>
                        <Typography paragraph>
                            {commentHeader}
                        </Typography>
                        <Typography paragraph className={classes.commentText}>
                        {comment.text}
                        </Typography>
                    </div>
                );
            });
        } 


        /*
        if (this.props.campaignItem.comments.length <= 0) {
            comments = <div className={classes.comment}><Typography paragraph>No comments</Typography></div>;
        } else {
            comments = this.props.campaignItem.comments.map((comment, index) => {
                let commentHeader = comment.username + " " + comment.created;
                return (
                    <div key={index} className={classes.comment}>
                        <Typography paragraph>
                            {commentHeader}
                        </Typography>
                        <Typography paragraph className={classes.commentText}>
                        {comment.text}
                        </Typography>
                    </div>
                );
            });
        }
        */

        /*

        <ReactPlayer 
                        playsinline
                        controls
                        width='100%'
                        height='100%'
                        url={this.props.campaignItem.video} />

                            <ReactPlayer 
                        width='100%'
                        height='100%'
                        controls={true}
                        url={this.props.campaignItem.video} />


                    <Player>
                        <source src={this.props.campaignItem.video}/>
                    </Player>

                    <Plyr
                        controls={['play','fullscreen']}
                        type="video"
                        url={this.props.campaignItem.video}
                    />

                    <Video
                        type='video/mp4'
                        width="100%"
                        height="100%"
                        controls={['PlayPause','Fullscreen','Seek']}
                    >
                        <source src={this.props.campaignItem.video}/>
                    </Video>

                    muted 
                    autoplay


                                        <CardMedia 
                        controls
                        component="video"
                        width='100%'
                        height='100%'
                        image={this.props.campaignItem.video} />


                    v käytä tuota!

                    <ReactPlayer 
                        playsinline
                        controls
                        width='100%'
                        height='100%'
                        url={this.props.campaignItem.video} />

        */


        //console.log(this.props.campaignItem);

        var path = "https://student.labranet.jamk.fi/~mapas/pwa/jaxber/data/";
        var avatar = path + "images/" + this.props.campaignItem.useravatar;
        var image = null;
        if (this.props.campaignItem.image !== null && this.props.campaignItem.image !== '') image = path + "images/"+this.props.campaignItem.image;
        var video = null;
        if (this.props.campaignItem.video !== null && this.props.campaignItem.video !== '') video = path + "videos/"+this.props.campaignItem.video;

        return (
            <div>
            <Card className={classes.card}>
                <CardHeader
                    avatar={
                        <Avatar className={classes.avatar} src={avatar}/>
                    }
                    action={
                        <IconButton onClick={this.handleCardMenuOpen}>
                        <MoreVertIcon />
                        </IconButton>
                    }
                    title={this.props.campaignItem.header}
                    subheader={subheader}
                />
                <Menu
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleCardMenuClose}
                >
                    <MenuItem onClick={this.openAddCommentDialog}>Add comment</MenuItem>
                </Menu>
                {image !== null &&
                    <CardContent>
                    <CardMedia
                        className={classes.image}
                        image={image}
                    />
                    </CardContent>
                }
                {video !== null &&
                    <CardContent>
                    <div className='classes.video'>
                    <ReactPlayer 
                        playsinline
                        controls
                        width='100%'
                        height='100%'
                        url={video} />
                    </div>
                    </CardContent>
                }
                <CardContent>
                <Typography component="p">
                    {this.props.campaignItem.text}
                </Typography>
                </CardContent>
                <CardActions className={classes.actions} disableActionSpacing>
                    <IconButton 
                        className={classnames({
                            [classes.favorite]: this.state.favorited,
                            })}
                        onClick={_ => this.favorite()}>
                        <FavoriteIcon />
                    </IconButton>
                    <IconButton
                        className={classnames(classes.expand, {
                        [classes.expandOpen]: this.state.expanded,
                        })}
                        onClick={_ => this.handleExpandClick(this.props.campaignItem.idcampaign)}
                        >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    {comments}
                </CardContent>
                </Collapse>
            </Card>
            <Dialog onClose={this.closeCommentDialog} open={this.state.commentDialogOpen}>
            <DialogTitle>Add a new comment</DialogTitle>
            <DialogContent>
                <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="commenttext" >Text</InputLabel>
                    <Input rows={4} multiline={true} id="commenttext" name="commenttext" onChange={this.updateCommenttextValue} value={this.state.commenttext} />
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={this.closeCommentDialog} color="primary">
                Cancel
                </Button>
                <Button onClick={this.addNewComment} color="primary">
                Add
                </Button>
            </DialogActions>
            <DialogContent className='classes.sendComment'>
                { this.state.isSendingComment && <p>{this.state.sendingMessage}</p> }
            </DialogContent>
            </Dialog>
            </div>
        );
    }
}

const styles = theme => ({
    sendComment: {
        marginLeft: '5px',
        marginRight: '5px'
    },
    commentText: {
        fontStyle: 'italic'
    },
    comment: {
        borderTop: '1px dotted gray',
        padding: '2px'
    },
    grow: {
        flexGrow: 1,
      },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        //marginLeft: theme.spacing.unit * 3,
        //marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
          width: 400,
          marginLeft: 'auto',
          marginRight: 'auto',
        },
      },
    card: {
      maxWidth: 400,
      marginTop:5
    },
    image: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    videoDiv: {
        position: 'relative',
        paddingTop: '56.25%'
    },
    video: {
        position: 'absolute',
        top: 0,
        left: 0    
    },
    actions: {
      display: 'flex',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    lockIcon: {
      float:'right'
    },
    avatar: {
        backgroundColor: red[500],
      },
    favorite: {
        color: red[500],
    }
  });

  CampaignCard.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(CampaignCard);

