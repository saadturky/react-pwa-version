import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import Grid from '@material-ui/core/Grid';
import LockIcon from '@material-ui/icons/Lock';
import AddIcon from '@material-ui/icons/Add';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import DialogContentText from '@material-ui/core/DialogContentText';

// dialog
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class Campaigns extends Component {
    constructor(props) {
        super(props);

        this.state = {
            campaignname: '',
            private: false
        };

        this.updateCampaignnameValue = this.updateCampaignnameValue.bind(this);
        this.updatePrivateValue = this.updatePrivateValue.bind(this);

        // start loading campaigns if not loaded yet
        if (this.props.campaigns.length === 0) this.props.loadCampaigns();
    }

    updatePrivateValue(e) {
        this.setState({ private: e.target.checked });    
    }

    updateCampaignnameValue(e) {
        this.setState({ campaignname: e.target.value });
    }  

    handleDialogOpen = () => {
        //this.setState({ dialogOpen: true });
        this.setState({ campaignname: '', private: false });
        this.props.showAddCampaignDialog(true);
    };
    
    handleDialogClose = () => {
        //this.setState({ dialogOpen: false });
        this.props.showAddCampaignDialog(false);
    };

    handleExpandClick = () => {
        this.setState(state => ({ expanded: !state.expanded }));
    };

    openDialog = () => {
        //this.props.history.push("/AddCampaign");
        this.handleDialogOpen();
    }

    addCampaign = () => {
        const moment = require('moment');
        // created
        let date = new Date();
        let created = moment(date.toISOString()).format('YYYY/MM/DD');

        this.props.addCampaign(
            {
                "created": created,
                "lastActivity": created,
                "name": this.state.campaignname,
                "private": this.state.private,
                "iduser": this.props.iduser,
                "username": this.props.username
            }
        );
    }

    // all cards just a load a same campaing!
    cardPressed = (index) => {
        //let { campaigns } = this.props;
        this.props.setSelectedCampaignIndex(index);
        this.props.history.push("/Campaign");
    }

    render() {
        const { classes } = this.props;
        let { isLoadingCampaignsPending, isLoadingCampaignsSuccess, loadingCampaignsError, 
            isAddCampaignPending, isAddCampaignSuccess, addCampaignError,
            campaigns,
            isAddCampaignsDialogOpen, 
            username
        } = this.props;

        var items = null;
        if (isLoadingCampaignsSuccess) {
            if (campaigns.length > 0) {
                items = campaigns.map((campaign, index) => {
                    const moment = require('moment');
                    // created
                    let date = new Date(campaign.created);
                    let created = moment(date.toISOString()).format('YYYY/MM/DD');
                    // last activity
                    let a = moment();
                    let lastDate = new Date(campaign.lastActivity);
                    let b = moment(lastDate.toISOString());
                    let lastActivity = moment.duration(a.diff(b)).humanize();
                    return (
                        <Card className={classes.card} key={index} onClick={_ => this.cardPressed(index)}>
                            <CardContent>
                                <Grid  container>
                                    <Grid item xs={9}>
                                        <Typography component="p">
                                            <b>{campaign.name}</b><br/>
                                            {campaign.username} {created}<br/>
                                            Last activity: {lastActivity}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={3}>
                                        {campaign.private === "1" &&
                                            <LockIcon className={classes.lockIcon}/>
                                        }
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </Card>
                    );
                  })}
        }

        return (
            <main className={classes.main}>

                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" onClick={_ => alert("Not implemented!")}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="overline" color="inherit" className={classes.grow}>
                            Jaxber - {username}
                        </Typography>
                        <IconButton color="inherit" onClick={_ =>this.openDialog()}>
                            <AddIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>


                {isLoadingCampaignsPending && 
                    <p>Loading campaigns, please wait...</p>
                }
                {loadingCampaignsError != null &&
                    <p>Campaigns loading error:<br/>- {loadingCampaignsError}</p>
                }
                { (isLoadingCampaignsSuccess && items == null) && 
                    <p>There is no campaigns!</p>
                }
                { (isLoadingCampaignsSuccess && items != null) && 
                    <List>
                        {items}
                    </List>
                }

                <Dialog
                    open={isAddCampaignsDialogOpen}
                    onClose={this.handleDialogClose}
                    aria-labelledby="form-dialog-title"
                    >
                    <DialogTitle id="form-dialog-title">Add a new Campaign</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Note this is just a test and created campaigns won't be saved!
                        </DialogContentText>
                        <FormControl margin="normal" required fullWidth>
                            <InputLabel htmlFor="campaignname">Campaign name</InputLabel>
                            <Input id="campaignname" name="campaignname" onChange={this.updateCampaignnameValue} value={this.state.campaignname} />
                        </FormControl>
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" onChange={this.updatePrivateValue} />}
                            label="Private"
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleDialogClose} color="primary">
                        Cancel
                        </Button>
                        <Button onClick={this.addCampaign} color="primary" disabled={this.state.campaignname === ''}>
                        Ok
                        </Button>
                    </DialogActions>
                    <DialogContent>
                    { isAddCampaignPending && <p>Adding, please wait...</p> }
                    { isAddCampaignSuccess && <p>Success.</p> }
                    { addCampaignError && <p>{addCampaignError.message}</p> }
                    </DialogContent>
                </Dialog>
            </main>
        )
    }
}

const styles = theme => ({
    grow: {
        flexGrow: 1,
      },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        //marginLeft: theme.spacing.unit * 3,
        //marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
          width: 400,
          marginLeft: 'auto',
          marginRight: 'auto',
        },
      },
    card: {
      maxWidth: 400,
      marginTop:5
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    actions: {
      display: 'flex',
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    lockIcon: {
      float:'right'
    },
  });

  Campaigns.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  

export default withStyles(styles)(Campaigns);
