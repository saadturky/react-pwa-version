import { 
  SET_LOADING_CAMPAIGNS_PENDING, 
  SET_LOADING_CAMPAIGNS_SUCCESS, 
  SET_LOADING_CAMPAIGNS_ERROR, 
  SET_SELECTED_CAMPAIGN_INDEX,
  SET_ADD_CAMPAIGN_SUCCESS, 
  SET_ADD_CAMPAIGN_PENDING,
  SET_SHOW_DIALOG
} from '../actions/types';

export default function campaignsReducer(
    state = {
      isLoadingCampaignsSuccess: false,
      isLoadingCampaignsPending: false,
      loadingCampaignsError: null,
      campaigns: [],
      selectedCampaignIndex: 0,
      isAddCampaignPending: false,
      isAddCampaignSuccess: false,
      addCampaignError: null,
      isAddCampaignsDialogOpen: false
  }, action) {
    switch (action.type) {
      case SET_LOADING_CAMPAIGNS_PENDING:
        return Object.assign({}, state, {
          isLoadingCampaignsPending: action.isLoadingCampaignsPending,
          isLoadingCampaignsSuccess: false,
          loadingCampaignsError: null
        });
  
      case SET_LOADING_CAMPAIGNS_SUCCESS:
        return Object.assign({}, state, {
          isLoadingCampaignsPending: false,
          isLoadingCampaignsSuccess: action.isLoadingCampaingsSuccess,
          loadingCampaignsError: null,
          campaigns: action.campaigns
        });
  
      case SET_LOADING_CAMPAIGNS_ERROR:
        return Object.assign({}, state, {
          loadingCampaignsError: action.loadingCampaignsError,
          isLoadingCampaignsPending: false,
          isLoadingCampaignsSuccess: false
        });
  
      case SET_SELECTED_CAMPAIGN_INDEX:
        return Object.assign({}, state, {
          selectedCampaignIndex: action.index
        });

      case SET_ADD_CAMPAIGN_SUCCESS:
        return {
          ...state,
          campaigns: [action.campaign, ...state.campaigns],
          isAddCampaignSuccess: true,
          isAddCampaignsDialogOpen: false
        }

      case SET_SHOW_DIALOG:
        return Object.assign({}, state, {
          isAddCampaignsDialogOpen: action.isOpen,
          isAddCampaignPending: false,
          isAddCampaignSuccess: false,
          addCampaignError: null
        });

      case SET_ADD_CAMPAIGN_PENDING:
        return Object.assign({}, state, {
          isAddCampaignPending: action.isAddCampaignPending,
          isAddCampaignSuccess: false,
          addCampaignError: null
        });

      default:
        return state;
    }
  }
