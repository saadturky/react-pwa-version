import { combineReducers } from 'redux';
import loginForm from './loginFormReducer';
import campaigns from './campaignsReducer';
import campaign from './campaignReducer';
import registerForm from './registerFormReducer'

export default combineReducers({
    registerForm: registerForm,
    loginForm: loginForm,
    campaigns: campaigns,
    campaign: campaign
});